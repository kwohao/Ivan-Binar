const Label = (props) => {
    const { htmlFor, children, variant = "text-gray-600" } = props;
    return (
      <label
        htmlFor={htmlFor}
        className={`block ${variant} text-sm font-bold mb-2`}
      >
        {children}
      </label>
    );
  };
  export default Label;
  