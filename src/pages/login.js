import React from "react";
import profile from "../image/profil.jpeg";
import email from "../image/message.jpeg";
import pass from "../image/pass.jpeg";
// import Button from "../components/button";
import { Link } from "react-router-dom";

function Login() {
  return (
    <div>
      <div className="main bg-slate-700 bg-auto w-full h-screen">
        <div className="sub-main">
          <div>
            <div className="imgs">
              <div className="container-image">
                <img src={profile} alt="profile" className="profile" />
              </div>
            </div>

            <div>
              <h1 class="text-5xl font-bold text-black-500 m-5 ">Login Page</h1>
              <div>
                <img src={email} alt="email" className="email" />
                <input type="text" placeHolder="Username" className="name" />
              </div>
              <div className="secondInput">
                <img src={pass} alt="pass" className="email" />
                <input
                  type="password"
                  placeHolder="Password"
                  className="name"
                />
              </div>
              <div className="mr-0">
                <button
                  className="bg-red-400 mt-4 h-10  rounded-full
             hover:bg-red-500 duration-300 w-full text-white font-semibold"
                >
                  LOGIN
                </button>
              </div>
            </div>

            <p className="link">
              <Link to={"/Registration"}>
                <p>Register</p>
              </Link>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Login;
