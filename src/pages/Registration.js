import React from "react";
import Button from "../components/button";
import { Link } from "react-router-dom";

function Registration() {
  return (
    <div className="bg-slate-700 h-screen flex justify-center items-center">
      <div className="bg-white  rounded-3xl shadow-2xl shadow-black/30  h-auto pl-10  w-[550px] flex flex-col justify-center items-center pr-10">
        <form className="space-y-5" action="">
          <div>
            <p className=" font-semibold text-center pt-10 text-4xl tracking-wide">
              Sign Up !
            </p>
          </div>
          <div className="mr-0">
            <p className="font-semibold text-2x1 tracking-wide">Username : </p>
            <input
              className="text-zinc-500 px-5 "
              type="text"
              placeholder="Username"
              required
            />
          </div>
          <div className="mr-0">
            <p className="font-semibold text-2x1 tracking-wide">Email :</p>
            <input
              className="text-zinc-500 px-5 "
              placeholder="Email"
              required
            />
          </div>
          <div className="mr-0">
            <p className="font-semibold text-2x1 tracking-wide">Password :</p>
            <input
              className="text-zinc-500 px-5  "
              type="Password"
              placeholder="Password"
              required
            />
          </div>
          <div className="mr-0">
            <button
              className="bg-red-400 h-10  rounded-full
             hover:bg-red-500 duration-300 w-full text-white font-semibold"
            >
              Sign Up
            </button>
          </div>
          <div>
            <p className="text-zinc-500 pb-10 text-center">
              Already Account Exist?{" "}
              <span className="text-black font-bold underline underline-offset-4">
                <Link to="/Login">SignIn</Link>
              </span>
            </p>
          </div>
        </form>
      </div>
    </div>
  );
}

export default Registration;
