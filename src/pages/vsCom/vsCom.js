import "./vsCom.css";
import Batu from "../../image/batu.png";
import Gunting from "../../image/gunting.png";
import Kertas from "../../image/kertas.png";
import Refresh from "../../image/refresh.png";
import PlayImage from "../../components/Fragments/PlayImage/PlayImage";
import { useState } from "react";

const VsCom = () => {
  const [choice, setChoice] = useState("");
  return (
    <div className="flex w-full h-screen bg-slate-700">
      <div className="flex w-2/5 h-full flex-row justify-center ">
        <div className="playerChoice">
          <h1 className="text-4xl font-semibold text-green-500 ml-3">
            PLAYER 1
          </h1>
          <PlayImage
            src={Batu}
            id="batu"
            classname={choice === "batu" ? "bg-white" : "bg-slate-700"}
            onClick={() => {
              if (choice === "") {
                setChoice("batu");
              }
            }}
          />
          <PlayImage
            src={Kertas}
            id="kertas"
            classname={choice === "kertas" ? "bg-white" : "bg-slate-700"}
            onClick={() => {
              if (choice === "") {
                setChoice("kertas");
              }
            }}
          />
          <PlayImage
            src={Gunting}
            id="gunting"
            classname={choice === "gunting" ? "bg-white" : "bg-slate-700"}
            onClick={() => {
              if (choice === "") {
                setChoice("gunting");
              }
            }}
          />
        </div>
      </div>

      <div className="flex w-1/5 h-full justify-center">
        <div>
          <div>
            <h1 className="judgement text-7xl font-bold text-green-500">VS</h1>
            <PlayImage
              src={Refresh}
              id="refresh"
              classname="refresher hover:border-white hover:bg-white mt-10"
              onClick={() => {
                if (choice !== "") {
                  setChoice("");
                }
              }}
            />
          </div>
        </div>
      </div>

      <div className="flex w-2/5 h-full justify-center">
        <div className="playerChoice ml-3">
          <h1 className="text-4xl text-mid font-semibold text-green-500">
            COMPUTER
          </h1>
          <img src={Batu} alt="" className="rpsChoice my-10 ml-10 " />
          <img src={Kertas} alt="" className="rpsChoice my-10 ml-10 " />
          <img src={Gunting} alt="" className="rpsChoice ml-10 " />
        </div>
      </div>
    </div>
  );
};
export default VsCom;
