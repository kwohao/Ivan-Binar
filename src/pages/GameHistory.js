const GameHistory = () => {
  return (
    <div className="flex w-full h-screen bg-slate-700">
      <div className="flex flex-col justify-center w-1/4 pb-1 px-10 mt-9 pt-8">
        <h1 className="mb-10 p-3 text-center text-3xl font-semibold text-white border-4 border-black rounded">
          VS COM
        </h1>
        <h1 className="mb-10 p-3 text-center text-3xl font-semibold text-white border-4 border-black rounded">
          VS COM
        </h1>
        <h1 className="mb-10 p-3 text-center text-3xl font-semibold text-white border-4 border-black rounded">
          ROOM A
        </h1>
        <h1 className="mb-10 p-3 text-center text-3xl font-semibold text-white border-4 border-black rounded">
          ROOM C
        </h1>
        <h1 className="mb-10 p-3 text-center text-3xl font-semibold text-white border-4 border-black rounded">
          ROOM D
        </h1>
      </div>

      <div className="flex w-2/4">
        <div className="flex w-full mt-10 justify-center">
          <div className="flex flex-col w-full text-center px-10">
            <h1 className="text-5xl font-semibold text-white mb-20">
              GAME HISTORY
            </h1>
            <h1 className="mb-10 mt-1 p-3 text-center text-3xl font-semibold text-white border-4 border-black rounded">
              6 JUNI 2023 22.36
            </h1>
            <h1 className="mb-10 p-3 text-center text-3xl font-semibold text-white border-4 border-black rounded">
              6 JUNI 2023 23.00
            </h1>
            <h1 className="mb-10 p-3 text-center text-3xl font-semibold text-white border-4 border-black rounded">
              7 JUNI 2023 00.36
            </h1>
            <h1 className="mb-10 p-3 text-center text-3xl font-semibold text-white border-4 border-black rounded">
              7 JUNI 2023 01.36
            </h1>
            <h1 className="mb-10 p-3 text-center text-3xl font-semibold text-white border-4 border-black rounded">
              7 JUNI 2023 01.48
            </h1>
          </div>
        </div>
      </div>

      <div className="flex w-1/4">
        <div className="flex flex-col justify-center w-full pb-1 px-10 mt-9 pt-8">
          <h1 className="mb-10 p-3 text-center text-3xl font-semibold text-white border-4 border-black rounded">
            WIN
          </h1>
          <h1 className="mb-10 p-3 text-center text-3xl font-semibold text-white border-4 border-black rounded">
            DRAW
          </h1>
          <h1 className="mb-10 p-3 text-center text-3xl font-semibold text-white border-4 border-black rounded">
            WIN
          </h1>
          <h1 className="mb-10 p-3 text-center text-3xl font-semibold text-white border-4 border-black rounded">
            LOSE
          </h1>
          <h1 className="mb-10 p-3 text-center text-3xl font-semibold text-white border-4 border-black rounded">
            LOSE
          </h1>
        </div>
      </div>
    </div>
  );
};

export default GameHistory;
