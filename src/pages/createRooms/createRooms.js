import "./createRooms.css";
import InputForm from "../../components/input/input";
import Button from "../../components/button";
import Batu from "../../image/batu.png";
import Gunting from "../../image/gunting.png";
import Kertas from "../../image/kertas.png";
import { useState } from "react";
import PlayImage from "../../components/Fragments/PlayImage/PlayImage";

const CreateRooms = () => {
  const [choice, setChoice] = useState("");
  return (
    <div className="flex w-full h-screen bg-gray-700">
      <div className="w-1/3">
        <div className="flex justify-center pilihan">
          <PlayImage
            src={Batu}
            id="batu"
            classname={choice === "batu" ? "bg-white" : "bg-slate-700"}
            onClick={() => {
              if (choice === "") {
                setChoice("batu");
              }
            }}
          />
        </div>
      </div>
      <div className="w-1/3">
        <form action="" className="mt-10">
          <InputForm
            name="roomname"
            type="text"
            placeholder="Field Room Name Here..."
            className="h-20 text-center"
            variant="text-orange-500 text-center text-4xl mt-10"
          />
        </form>
        <div className="flex justify-center kertas">
          <PlayImage
            src={Kertas}
            id="kertas"
            classname={choice === "kertas" ? "bg-white" : "bg-slate-700"}
            onClick={() => {
              if (choice === "") {
                setChoice("kertas");
              }
            }}
          />
        </div>
        <Button variant="bg-red-500 tombolPilihan w-60 h-20 text-3xl hover:bg-orange-600 hover:scale-110 hover:cursor-pointer transition-all">
          Choose
        </Button>
      </div>
      <div className="w-1/3 ">
        <div className="flex justify-center pilihan">
          <PlayImage
            src={Gunting}
            id="gunting"
            classname={choice === "gunting" ? "bg-white" : "bg-slate-700"}
            onClick={() => {
              if (choice === "") {
                setChoice("gunting");
              }
            }}
          />
        </div>
      </div>
    </div>
  );
};

export default CreateRooms;
